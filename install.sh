#https://www.arangodb.com/download-major/ubuntu/
#Docker sudo docker run -p 8529:8529 -e ARANGO_ROOT_PASSWORD=openSesame arangodb/arangodb:3.7.12
echo "Installing FunkDecoder"
echo "by @woehrer"
echo ""

echo "(1/x) Installing Software"
git clone https://github.com/woehrer12/skripte.git ~/skripte
sudo apt-get update >> logs/install.log
sudo apt-get install python3 python3-pip cmake libusb-1.0 librtlsdr-dev libtool libfftw3-dev -y >> logs/install.log
pip3 install --upgrade python-telegram-bot geojson OSMPythonTools aprslib django >> logs/install.log

echo "(2/x) Git Pull"
git pull >> logs/install.log

echo "(3/x) Installing ArangoDB"
echo "0 = Install ArangoDB locally"
echo "1 = Install ArangoDB on Docker"
echo "2 = Install ArangoDB on Docker for Raspberry (only for development)"
echo "3 = nothing"
echo "enter your choice"
read -r choice
if [ $choice -eq 0 ]; then
    echo "Installing ArangoDB locally"
    sudo apt-get install -y arangodb3 >> logs/install.log 2>&1
    sudo systemctl start arangodb >> logs/install.log 2>&1
    sudo systemctl enable arangodb >> logs/install.log 2>&1
    sudo systemctl status arangodb >> logs/install.log 2>&1
    echo "ArangoDB installed"
elif [ $choice -eq 1 ]; then
    echo "Installing ArangoDB on Docker"
    sudo docker run -e ARANGO_ROOT_PASSWORD=openSesame -p 8529:8529 -d arangodb/arangodb >> logs/install.log 2>&1
    echo "ArangoDB installed on Docker"
elif [ $choice -eq 2 ]; then
    echo "Installing ArangoDB on Raspberry"
    sudo docker run -e ARANGO_ROOT_PASSWORD=openSesame -p 8529:8529 -d programmador/arangodb:3.8.0-devel >> logs/install.log 2>&1
    echo "ArangoDB installed on Raspberry"
elif [$1 -eq 3]; then
    echo "nothing"
fi

echo "(4/x) Installing RTL_FM"
git clone https://github.com/osmocom/rtl-sdr.git >> logs/install.log 2>&1
cd rtl-sdr/
mkdir -p build
cd build
cmake .. 
make 
sudo make install 
sudo ldconfig 
cd ..
cd ..

echo "(5/x) Installing multimon-ng"
git clone https://github.com/EliasOenal/multimon-ng.git >> logs/install.log 2>&1
cd multimon-ng/
mkdir -p build
cd build
cmake ..
make
sudo make install
cd ..
cd ..

echo "(6/x) Blacklist the DVBT Driver"
echo 'blacklist dvb_usb_rtl28xxu' | sudo tee --append /etc/modprobe.d/blacklist-dvb_usb_rtl28xxu.conf >> logs/install.log 2>&1

echo "(7/x) Installing Kalibrate-rtl"
git clone https://github.com/hayguen/kalibrate-rtl.git >> logs/install.log 2>&1
cd kalibrate-rtl/
bash bootstrap
bash configure
make
sudo make install
cd ..

echo "FunkDecoder is now installed"