#!/bin/bash -e
on_chroot << EOF
    git clone https://github.com/woehrer12/skripte.git ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/skripte
    chown pi:pi /home/${FIRST_USER_NAME}/skripte
    cd ${ROOTFS_DIR}/home/${FIRST_USER_NAME}/skripte
    ./raspberry/install_display.sh
EOF

chmod +x 02-run.sh
