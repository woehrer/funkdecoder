#!/bin/bash -e
on_chroot << EOF
    echo "@reboot /home/pi/skripte/updater.sh" | crontab -
    su pi
    echo "@reboot /home/pi/funkdecoder/raspberry/job.sh" | crontab -
    git clone https://github.com/woehrer12/skripte.git /home/${FIRST_USER_NAME}/skripte
    git clone https://gitlab.com/woehrer/funkdecoder.git /home/${FIRST_USER_NAME}/funkdecoder
    chown -R pi:pi /home/${FIRST_USER_NAME}/skripte
    chown -R pi:pi /home/${FIRST_USER_NAME}/funkdecoder
    cd /home/${FIRST_USER_NAME}/funkdecoder
    ./install.sh 3
    skripte/docker-install.sh
EOF

chmod +x 04-run.sh

#     chmod +x ./install.sh
#     chmod +x ./start.sh