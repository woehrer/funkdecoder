from django.apps import AppConfig


class TelegrammeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'telegramme'
