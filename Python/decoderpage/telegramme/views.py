from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic import DetailView

from .models import Telegramme

class TelegrammeHome(ListView):
    model = Telegramme
    template_name = 'telegramme/telegramme_home.html'

class TelegrammeDetail(DetailView):
    model = Telegramme
    template_name = 'telegramme/telegramme_detail.html'
