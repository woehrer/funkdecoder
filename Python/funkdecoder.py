#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import helper.config
import helper.functions
import helper.decoder
import helper.filter
import time
import subprocess
import plugin.helper
import sys

#Inizialisierung der Konfiguration
conf = helper.config.initconfig()
helper.functions.initlogger()

# Starte FunkDecoder
try:
    logging.debug("Starte FunkDecoder")
    print("==Starte FunkDecoder==\n")
    helper.config.check()

except Exception as e:
    logging.error("Fehler: " + str(e))
    print("Fehler: " + str(e))
    exit(1)

# Starte RTL_FM
try:
    logging.debug("Starte RTL_FM")
    print("==Starte RTL_FM==")
    with open("logs/rtl_fm.log","w") as rtl_fm_log:
        rtl_fm_log.write("Starte RTL_FM")
    command = "/usr/local/bin/rtl_fm -d " + \
              str(conf['DEVICE']) + \
              " -f "+ \
              str(conf['FREQ']) + \
              " -M fm -p " + \
              str(conf['PPM_ERROR']) + \
              " -E DC -F 0 -l " + \
              str(conf['SQUELCH']) + \
              " -g " + \
              str(conf['GAIN']) + \
              " -s 22050"
    logging.debug(command)
    rtl_fm = subprocess.Popen(command.split(),
        stdout=subprocess.PIPE,
        stderr=open("logs/rtl_fm.log","a"),
        shell=False)
    time.sleep(3)
    helper.functions.checkRTL()
    logging.debug("RTL_FM gestartet")
    print("RTL_FM gestartet\n")

except Exception as e:
    logging.error("Fehler beim Start von RTL_FM: " + str(e))
    print("Fehler beim Start von RTL_FM: " + str(e))
    exit(1)

# Starte Multimon-ng
try:
    logging.debug("Starte multimon-ng")
    print("==Starte multimon-ng==")
    with open("logs/multimon.log","w") as multimon_log:
        multimon_log.write("Starte multimon-ng\n")
    demodulation = ""
    if conf['FMS'] == "True":
        demodulation += "-a FMSFSK "
        logging.debug(" - Demod: FMS")
    if conf['ZVEI'] == "True":
        demodulation += "-a ZVEI1 "
        logging.debug(" - Demod: ZVEI")
    if conf['POC512'] == "True":
        demodulation += "-a POCSAG512 "
        logging.debug(" - Demod: POC512")
    if conf['POC1200'] == "True":
        demodulation += "-a POCSAG1200 "
        logging.debug(" - Demod: POC1200")
    if conf['POC2400'] == "True":
        demodulation += "-a POCSAG2400 "
        logging.debug(" - Demod: POC2400")
    if conf['APRS'] == "True":
        demodulation += "-A -a AFSK1200 "
        logging.debug(" - Demod: APRS")
    if conf['dapnet'] == "True":
        demodulation += " -a POCSAG1200 "
        logging.debug(" - Demod: DAPNET")
    command = "/usr/local/bin/multimon-ng "+str(demodulation)+" -f alpha -t raw /dev/stdin - "
    logging.debug(command)
    multimon_ng = subprocess.Popen(command.split(),
        stdin=rtl_fm.stdout,
        stdout=subprocess.PIPE,
        stderr=open("logs/multimon.log","a"),
        shell=False)
    time.sleep(3)
    helper.functions.checkMultimon()
    logging.debug("multimon-ng gestartet")
    print("multimon-ng gestartet\n")

except Exception as e:
    logging.error("Fehler beim Start von Multimon: " + str(e))
    print("Fehler beim Start von Multimon: " + str(e))
    exit(1)

try:
    logging.debug("Start Decoder")
    print("==Start Decoder==")
    while True:
        line = str(multimon_ng.stdout.readline().decode('utf-8'))
        with open("logs/rawmultimon.log","a") as rawmultimon:
            rawmultimon.write(line)
        decoded = helper.decoder.decode(line)
        if decoded != False:
            plugin.helper.callplugins(decoded)

except KeyboardInterrupt:
    logging.debug("Beende Decoder")
    print("==Beende Decoder==")
    rtl_fm.kill()
    multimon_ng.kill()
    time.sleep(3)
    logging.debug("Decoder beendet")
    print("Decoder beendet\n")
    exit(0)

except Exception as e:
    logging.error("Fehler beim decoden: " + str(e))
    print("Fehler beim decoden: " + str(e))
    rtl_fm.kill()
    multimon_ng.kill()
    exit(1)