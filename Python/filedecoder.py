import logging
import helper.decoder
import helper.filter
import time
import plugin.helper



try:
    logging.debug("Start FileDecoder")
    print("==Start FileDecoder==")
    rawmultimon = open("logs/rawmultimon.log","r")
    lines = rawmultimon.readlines()
    for line in lines:
        decoded = helper.decoder.decode(line)
        if decoded != False:
            plugin.helper.callplugins(decoded)
        time.sleep(1)

except KeyboardInterrupt:
    logging.debug("Beende Decoder")
    print("==Beende Decoder==")
    logging.debug("Decoder beendet")
    print("Decoder beendet\n")
    exit(0)

except Exception as e:
    logging.error("Fehler beim decoden: " + str(e))
    print("Fehler beim decoden: " + str(e))
    exit(1)
