import plugin.telegram
import plugin.email
import plugin.aprs
import helper.config
import helper.filter
import helper.sql
import helper.doubleFilter

conf = helper.config.initconfig()

def callplugins(decoded):
    filtered = helper.filter.filter(decoded)
    if filtered != False and helper.doubleFilter.checkID(decoded):
        if conf['telegramplugin'] == "True":
            plugin.telegram.send(decoded)
        if conf['emailplugin'] == "True":
            plugin.email.send(decoded)
        if conf['aprsplugin'] == "True" and decoded['protocol'] == "APRS":
            plugin.aprs.send(decoded)
        if conf['sqlplugin'] == "True":
            helper.sql.insert(decoded)
        if "POCSAG" in decoded['protocol']:
            print(decoded['msg'], decoded['address'])
        if "APRS" in decoded['protocol']:
            print(decoded['from'])
            print(decoded['via'] + str(decoded['path']))
            if 'comment' in decoded:
                print(decoded['comment'])
            if 'text' in decoded:
                print(decoded['text'])
        print("")
