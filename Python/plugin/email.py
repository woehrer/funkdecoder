import logging
import sys
import os
import smtplib, ssl
import helper.config
import helper.functions

import logging.handlers

#Inizialisierung der Konfiguration
conf = helper.config.initconfig()

def send(data):
    mailhost = conf['mailhost']
    mailport = conf['mailport']
    mailuser = conf['mailuser']
    password = conf['mailpassword']
    toaddrs = conf['toaddrs']


    # write errors to email
    error_mail_subject = "ERROR: Script error in %s on %s" % (sys.argv[0], os.uname())
    error_mail_handler = logging.handlers.SMTPHandler(mailhost=mailhost,
                                                        fromaddr=mailuser, 
                                                        toaddrs=toaddrs, 
                                                        subject= error_mail_subject,
                                                        credentials=(mailuser,password),
                                                        secure=(),
                                                        timeout=10.0)
    error_mail_handler.setLevel(logging.ERROR)
    #error_mail_handler.setLevel(logging.DEBUG)

    # buffer debug messages so they can be sent with error emails
    memory_handler = logging.handlers.MemoryHandler(1024*10, logging.ERROR, error_mail_handler)
    memory_handler.setLevel(logging.DEBUG)


    message = "\
    Subject: Alarm" + "\n\n"
    message += "Funkdecoder: " + data['msg'] + "\n"
    message += "Frequency: " + data['freq'] + "\n"
    if data['geo']:
        message += 'https://www.google.com/maps?q={},{}&z=14'.format(data['lat_float'], data['lon_float'])

    with smtplib.SMTP_SSL(mailhost, mailport) as smtp:
        smtp.login(mailuser, password)
        smtp.sendmail(mailuser,toaddrs, message)
