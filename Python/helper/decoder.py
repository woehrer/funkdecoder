import logging
import helper.config
import helper.functions
import time
import re
import helper.osm
import aprslib

conf = helper.config.initconfig()

def decode(data):
    try:
        decoded = {}
        decoded['raw'] = data
        data = data.replace("~","ß").replace("}","ü").replace("{","ä").replace("|","ö")
        decoded['EPOCH'] = time.time()
        decoded['TIMESTAMP'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        if "Enabled demodulators:" in data:
            return False
        elif "FMS" in data:
            decoded['protocol'] = "FMS"
            print("FMS")
        elif "ZVEI" in data:
            decoded['protocol'] = "ZVEI"
            print("ZVEI")
        elif "POCSAG" in data and conf['dapnet'] == 'True':
            decoded['protocol'] = "DAPNET"
            decoded['protocol'] = "POCSAG"
            decoded['type'] = ""
            decoded['encoded'] = False
            decoded['nomsg'] = False
            decoded['freq'] = conf['FREQ']
            decoded['geo'] = False
            if "Address: " in data:
                address = data.split("Address: ")[1].split("Function: ")[0]
                decoded['address'] = address.replace(" ","")
                networkaddress = conf['network_address'].split(",")
                if decoded['address'] in networkaddress:
                    decoded['type'] = "network"
            if "Alpha:" in data:
                alpha = data.split("Alpha:")[1]
                decoded['msg'] = alpha.lstrip()
        elif "POCSAG512" in data or "POCSAG1200" in data or "POCSAG2400" in data:
            decoded['protocol'] = "POCSAG"
            decoded['type'] = ""
            decoded['encoded'] = False
            decoded['nomsg'] = False
            decoded['freq'] = conf['FREQ']
            decoded['geo'] = False
            if "POCSAG512" in data:
                decoded['protocol'] = "POCSAG512"
            if "POCSAG1200" in data:
                decoded['protocol'] = "POCSAG1200"
            if "POCSAG2400" in data:
                decoded['protocol'] = "POCSAG2400"
            if "Address: " in data:
                address = data.split("Address: ")[1].split("Function: ")[0]
                decoded['address'] = address.replace(" ","")
                networkaddress = conf['network_address'].split(",")
                if decoded['address'] in networkaddress:
                    decoded['type'] = "network"
            if "Function: " in data:
                function = data.split("Function: ")[1].split("  ")[0]
                decoded['function'] = function
            if "Alpha:" in data:
                alpha = data.split("Alpha:")[1]
                decoded['msg'] = alpha.lstrip()
                var = [ 
                "<SUB>" ,
                "<FF>",
                "<SI>" ,
                "<ENQ>",
                "<NAK>",
                "<STX>",
                "<ETX>",
                "<ACK>" ,
                "<DLE>" ,
                "<GS>" ,
                "<CAN>" ,
                "<ETB>" ,
                "<DC1>" ,
                "<DC2>" ,
                "<DC3>" ,
                "<DLE>" ,
                "<EOT>" ,
                ".7kZ."
                ]
                inlist = [x for x in var if x in alpha]

                if any(inlist):
                    decoded['encoded'] = True
                else:
                    # TODO convert from config

                    geo = re.search("#K01;N(\d{7})E(\d{7});", decoded['msg'])
                    if geo:
                        LAT = re.findall("N(\d{2})(\d{5})", decoded['msg'])
                        decoded['LAT'] = LAT[0][0]
                        decoded['lat'] = LAT[0][1]
                        decoded['lat_float'] = float(decoded['LAT'] + "." + decoded['lat'])
                        LON = re.findall("E(\d{2})(\d{5})", decoded['msg'])
                        decoded['LON'] = LON[0][0]
                        decoded['lon'] = LON[0][1]
                        decoded['lon_float'] = float(decoded['LON'] + "." + decoded['lon'])
                        decoded['geo'] = True
                        decoded['msg'] = decoded['msg'].split(";")[2]

                    split = decoded['msg'].split(",")
                    if len(split) > 3:
                        decoded['ident'] = split[0]
                        decoded['city'] = split[1]
                        decoded['street'] = split[2]
                        decoded['name'] = split[3]
                        try:
                            osm_text = decoded['city'] + " " + decoded['street']
                            address = helper.osm.searchaddressOSM(osm_text)
                            if len(address) > 0:
                                if "lat" in address[0] and decoded['geo'] == False:
                                    decoded['lat_float'] = address[0]['lat']
                                    decoded['lon_float'] = address[0]['lon']
                                    decoded['geo'] = True
                                    

                        except:
                            logging.info("OSM Plugin fehlgeschlagen")
                            logging.info(decoded)
                            print("OSM Plugin fehlgeschlagen")

            else:
                decoded['nomsg'] = True
        elif "APRS" in data:
            decoded['protocol'] = "APRS"
            decoded['freq'] = conf['FREQ']
            decoded['geo'] = False
            decoded['EPOCH'] = time.time()
            decoded['TIMESTAMP'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            data = data.replace("APRS: ","")
            try:
                aprsdecoded = aprslib.parse(data)
                for key in aprsdecoded:
                    decoded[key] = aprsdecoded[key]
            except Exception as e:
                logging.info("APRS Plugin fehlgeschlagen: " + str(e))
                logging.info(decoded)
                print("APRS Plugin fehlgeschlagen: " + str(e))
                with open('logs/aprsparserror.log', 'a') as aprsparserror:
                    aprsparserror.write(data)
                    aprsparserror.write(str(e) + '\n\n')
                return False
            if 'latitude' in decoded:
                decoded['lat_float'] = decoded['latitude']
                decoded['lon_float'] = decoded['longitude']
                decoded['geo'] = True
            decoded['msg'] = decoded['from']
            if 'comment' in decoded:
                decoded['msg'] = decoded['msg'] + "\n" + decoded['comment']
        return decoded

    except Exception as e:
        logging.info("Decode Plugin fehlgeschlagen: " + str(e))
        logging.info(decoded)
        print("Decode Plugin fehlgeschlagen: " + str(e))
