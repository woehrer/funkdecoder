import sqlite3

def insert(decoded):
    sqldecoded = {'raw': "",
                  'freq': "",
                  'EPOCH': 0.0,
                  'TIMESTAMP': "",
                  'protocol': "",
                  'type': "",
                  'encoded': False,
                  'nomsg': False,
                  'geo': False,
                  'address': "",
                  'function': "",
                  'msg': "",
                  'lat': 0.0,
                  'lon': 0.0,
                  'ident': "",
                  'city': "",
                  'street': "",
                  'name': "",
                  'fromm': "",
                  'to': "",
                  'via': "",
                  'path': "",
                  'format': "",
                  'telemetry_message': "",
                  'comment': "",
                  'text': ""}

    for key in decoded:
        sqldecoded[key] = decoded[key]
    
    verbindung = sqlite3.connect("Python/decoderpage/db.sqlite3")
    cursor = verbindung.cursor()
    execute = """INSERT INTO telegramme_telegramme
    (raw, freq, epoch, timestamp, protocol, type, encoded, nomsg, geo, address, function, msg,
    lat, lon, ident, city, street, name, fromm, too, via, path, format, telemetry_message, comment, text)
    VALUES ('{}', '{}', {}, '{}', '{}', '{}', {} , {}, {}, '{}', '{}', '{}', {}, {}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', "{}", '{}', '{}', '{}', '{}')"""\
    "".format(sqldecoded['raw'].replace('"', '').replace('(', '').replace(')', '').replace('\'', '').replace('?', ''), 
    sqldecoded['freq'], sqldecoded['EPOCH'], sqldecoded['TIMESTAMP'], sqldecoded['protocol'], sqldecoded['type'],
    sqldecoded['encoded'], sqldecoded['nomsg'], sqldecoded['geo'], sqldecoded['address'], sqldecoded['function'], 
    sqldecoded['msg'].replace('"', '').replace('(', '').replace(')', '').replace('\'', '').replace('?', ''), 
    sqldecoded['lat'], sqldecoded['lon'], sqldecoded['ident'], sqldecoded['city'], sqldecoded['street'], sqldecoded['name'], sqldecoded['fromm'], sqldecoded['to'], sqldecoded['via'], sqldecoded['path'], sqldecoded['format'], sqldecoded['telemetry_message'], sqldecoded['comment'], sqldecoded['text'])
    cursor.execute(execute)
    verbindung.commit()
    verbindung.close()


# https://www.sqlitetutorial.net/sqlite-tutorial/sqlite-show-tables/
