import logging
import os

def initlogger():
	if not os.path.isdir("./logs"):
		os.mkdir("./logs")
	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)
	handler = logging.FileHandler("logs/FunkDecoder.log")
	formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
	handler.setFormatter(formatter)
	handler.setLevel(logging.DEBUG)
	logger.addHandler(handler)

def checkRTL():
	try:
		rtlLog = open("logs/rtl_fm.log","r").read()
		if ("exiting" in rtlLog) or  ("Failed to open" in rtlLog) or ("can't open" in rtlLog) or ("No supported devices" in rtlLog):
			logging.debug("\n%s", rtlLog)
			raise OSError("starting rtl_fm returns an error")
	except OSError:
		raise
	except:
		# we couldn't work without rtl_fm
		logging.critical("cannot check rtl_fm.log")
		logging.debug("cannot check rtl_fm.log", exc_info=True)
		raise

def checkMultimon():
	try:
		multimonLog = open("logs/multimon.log","r").read()
		if ("invalid" in multimonLog) or ("error" in multimonLog) or ("can't open" in multimonLog):
			logging.debug("\n%s", multimonLog)
			raise OSError("starting multimon-ng returns an error")
	except OSError:
		raise
	except:
		# we couldn't work without multimon-ng
		logging.critical("cannot check multimon.log")
		logging.debug("cannot check multimon.log", exc_info=True)
		raise