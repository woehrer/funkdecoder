sudo apt-get update
sudo apt-get install coreutils quilt parted qemu-user-static debootstrap zerofree zip \
dosfstools libarchive-tools libcap2-bin grep rsync xz-utils file git curl bc \
qemu-utils kpartx -y
git clone https://github.com/RPi-Distro/pi-gen.git
cp raspberry/config pi-gen/
cp -r raspberry/04-funkdecoder/ pi-gen/stage2/
cp -r raspberry/02-LCD/ pi-gen/stage3/
cd pi-gen
sudo docker rm -v pigen_work
sudo ./build-docker.sh
sudo docker rm -v pigen_work
